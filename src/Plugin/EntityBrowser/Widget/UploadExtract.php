<?php

namespace Drupal\eb_upload_extract\Plugin\EntityBrowser\Widget;

use Drupal\entity_browser\Plugin\EntityBrowser\Widget\Upload;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Utility\Token;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\File\FileSystem;
use Drupal\media\MediaInterface;
use Drupal\file\Entity\File;

/**
 * Uses a view to provide entity listing in a browser's widget.
 *
 * @EntityBrowserWidget(
 *   id = "upload_extract",
 *   label = @Translation("Upload and Extract"),
 *   description = @Translation("Adds an upload field browser's widget."),
 *   auto_select = FALSE
 * )
 */
class UploadExtract extends Upload {

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Upload constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The Form Builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
  EventDispatcherInterface $event_dispatcher,
  EntityTypeManagerInterface $entity_type_manager,
  WidgetValidationManager $validation_manager,
  ModuleHandlerInterface $module_handler,
  Token $token,
  FileSystem $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager, $module_handler, $token);
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('module_handler'),
      $container->get('token'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entity_type' => 'file',
      'media_type' => 'image',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);
    $form['upload']['#description'] = $this->t('If a zip file is provided, it will be automatically extracted');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    //$files = parent::prepareEntities($form, $form_state
    $files = [];

    foreach ($form_state->getValue(['upload'], []) as $fid) {
      $file = $this->entityTypeManager->getStorage('file')->load($fid);
      // Check if incoming file is a compressed (zip) file
      if(strpos($file->getFileName(), '.zip') !== 0) {
        $user = \Drupal::currentUser();
        // Get file's filesystem path
        $archived_file_path = $this->fileSystem->realpath($file->getFileUri());
        $extract_dir_uri = dirname($file->getFileUri());
        // @TODO allow the user to select the
        $extract_dir = $this->fileSystem->realpath($extract_dir_uri) . '/';

        // Load the appropriate archiver and extarct.
        // @TODO archiver should wrap through a common interface. ZipArchive is
        // the default for now
        $archiver = $this->getArchiver($file);
        if (method_exists($archiver, 'open')) {
          $res = $archiver->open($archived_file_path);
          if ($res === TRUE) {
            // Extract files
            for( $i = 0; $i < $archiver->numFiles; $i++ ){
                $stat = $archiver->statIndex( $i );
                if ($stat['name'] == '/') {
                  continue;
                }
                $result = $archiver->extractTo($extract_dir, $stat['name']);
                if (!$result) {
                  $messenger = \Drupal::messenger();
                  $messenger->addMessage(
                    $this->t('File @name could not be extracted.', ['@name' => $stat['name']]),
                    $messenger::TYPE_ERROR);
                } else {
                  $store_file = File::create([
                    'uri' => $extract_dir_uri . '/' . $stat['name'],
                    'uid' => $user->id(),
                    'status' => FILE_STATUS_PERMANENT,
                  ]);
                  $store_file->save();

                  $entity_type = $this->configuration['entity_type'];
                  if ($entity_type == 'media') {

                    /** @var \Drupal\media\MediaTypeInterface $media_type */
                    $media_type = $this->entityTypeManager
                      ->getStorage('media_type')
                      ->load($this->configuration['media_type']);

                    /** @var \Drupal\media\MediaInterface $media */
                    $media = $this->entityTypeManager->getStorage('media')->create([
                      'bundle' => $media_type->id(),
                      $media_type->getSource()->getConfiguration()['source_field'] => $store_file,
                    ]);
                    $media->save();
                    $files[] = $media;
                  } else {
                    $files[] = $store_file;
                  }
                }
            }
            $archiver->close();
          }
        }
      } else {
        $files[] = $file;
      }
    }
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getTriggeringElement()['#eb_widget_main_submit'])) {
      $files = $this->prepareEntities($form, $form_state);
      $this->selectEntities($files, $form_state);
      $this->clearFormValues($element, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['entity_type'] = [
      '#title' => $this->t('Entity Type'),
      '#type' => 'select',
      '#default_value' => $this->configuration['entity_type'],
      '#options' => [
        'file' => $this->t('File'),
        'media' => $this->t('Media'),
      ],
      '#prefix' => '<div class="et-control">',
      '#suffix' => '</div>'
    ];

    $media_types = $this->entityTypeManager
      ->getStorage('media_type')->loadMultiple();

    $media_type_options = [];
    foreach($media_types as $type_name => $type) {
      $media_type_options[$type_name] = $type->label();
    }

    $form['media_type'] = [
      '#title' => $this->t('Entity Type'),
      '#type' => 'select',
      '#default_value' => $this->configuration['media_type'],
      '#options' => $media_type_options,
      '#states' => array(
        'visible' => array(
          '.et-control select' => array('value' => 'media'),
        )
      )
    ];

    return $form;
  }

  /**
   * Get an appropriate archiver class for the file.
   *
   * @param string $file
   *   The file path.
   */
  public function getArchiver($file) {
    $extension = strstr($file->getFileName(), '.');
    switch ($extension) {
      // @TODO: add support for .tar and .tar.gz
      /*case '.tar.gz':
      case '.tar':
        $archiver = new \PharData($file);
        break;*/

      case '.zip':
        $archiver = new \ZipArchive($file);
      default:
        break;
    }
    return $archiver;
  }
}
